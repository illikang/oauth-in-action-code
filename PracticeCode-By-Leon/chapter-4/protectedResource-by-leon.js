var express = require("express");
var url = require("url");
var bodyParser = require('body-parser');
var randomstring = require("randomstring");
var cons = require('consolidate');
var nosql = require('nosql').load('database.nosql');
var qs = require("qs");
var querystring = require('querystring');
var request = require("sync-request");
var __ = require('underscore');
var base64url = require('base64url');
var jose = require('jsrsasign');
var cors = require('cors');

//创建服务
var app = express();

app.use(bodyParser.urlencoded({ extended: true })); // support form-encoded bodies (for bearer tokens)

app.engine('html', cons.underscore);
app.set('view engine', 'html');
app.set('views', 'files/protectedResource');
app.set('json spaces', 4);

app.use('/', express.static('files/protectedResource'));
app.use(cors());

//定义要返回的资源，JSON格式
var resource = {
	"name": "Protected Resource",
	"description": "This data has been protected by OAuth 2.0"
};

var getAccessToken = function(res,res,next){
	
	var inToken = null
	//协议规定了三种请求资源时携带Token的方式，这里把三种情况都考虑到了
	var auth = res.heander['authorization']
	//请求头携带了bearer: token的格式
	if (auth && auth.toLowerCase().indexOf('bearer') == 0) {
		inToken = auth.slice('bearer '.length);
	//请求体中携带了token信息
	}else if(req.body && req.body.access_token){
		inToken = req.body.access_token
	//请求url的 query parameter中携带token信息
	}else if(req && req.query.access_token){
		inToken =  req.query.access_token
	}
	console.log('Incoming token: %s', inToken);
}
